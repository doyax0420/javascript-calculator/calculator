let inputDisplay = document.querySelector("#txt-input-display");
//document.getElementsByClassName("btn-numbers");
let btnNumbers = document.querySelectorAll('.btn-numbers');
let btnAdd = document.querySelector("#btn-add");
let btnSubtract = document.querySelector("#btn-subtract");
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');

let btnClearAll = document.querySelector('#btn-clear-all');
let btnBackspace = document.querySelector('#btn-backspace');

//instantiate values
let numberA = null;
let numberB = null;
let operation = null;

/*function getNum(){
	console.log(inputDisplay.value);
}

for(let i = 0; i < btnNumbers.length; i++){
	btnNumbers[i].addEventListener("click", getNum);
}*/

btnNumbers.forEach(function(btnNumber){
	btnNumber.onclick = () => {
		inputDisplay.value += btnNumber.innerText;
	}
})

// old way
/*function functionName(){

}
//Cool new way 
const functionName = () => {
	//code
	//arrow functions can omit () if there is 1 paramenter
	//can also omit {} if only 1 statement
}
*/
btnAdd.onclick = () => {
	if(numberA === null){
		numberA = Number(inputDisplay.value);
		operation = 'addition';
		inputDisplay.value = null;
	}else if(numberB === null){
		numberB = Number(inputDisplay.value);
		numberA =  numberA + numberB;
		operation = 'addition';
		numberB = null;
		inputDisplay.value = null;
	}
}

btnSubtract.onclick = () => {
	if(numberA === null){
		numberA = Number(inputDisplay.value);
		operation = 'subtraction';
		inputDisplay.value = null;
	}else if(numberB === null){
		numberB = Number(inputDisplay.value);
		numberA = numberA - numberB;
		operation = 'subtraction';
		numberB = null;
		inputDisplay.value = null;
	}
}

btnMultiply.onclick = () => {
	if(numberA === null){
		numberA = Number(inputDisplay.value);
		operation = 'multiplication';
		inputDisplay.value = null;
	}else if(numberB === null){
		numberB = Number(inputDisplay.value);
		numberA = numberA * numberB;
		operation = 'multiplication';
		numberB = null;
		inputDisplay.value = null;
	}
}

btnDivide.onclick = () => {
	if(numberA === null){
		numberA = Number(inputDisplay.value);
		operation = 'division';
		inputDisplay.value = null;
	}else if(numberB === null){
		numberB = Number(inputDisplay.value);
		numberA = numberA / numberB;
		operation = 'division';
		numberB = null;
		inputDisplay.value = null;
	}
}


btnEqual.onclick = () => {
	if(numberB === null && inputDisplay.value !== ''){
		numberB = inputDisplay.value;
	}

	if(operation === 'addition'){
		numberA = Number(numberA) + Number(numberB);
		inputDisplay.value = numberA;
		numberA = null;
		numberB = null;
	}

	if(operation === 'subtraction'){
		numberA = Number(numberA) - Number(numberB);
		inputDisplay.value = numberA;
		numberA = null;
		numberB = null;
	}

	if(operation === 'multiplication'){
		numberA = Number(numberA) * Number(numberB);
		inputDisplay.value = numberA;
		numberA = null;
		numberB = null;
	}

	if(operation === 'division'){
		numberA = Number(numberA) / Number(numberB);
		inputDisplay.value = numberA;
		numberA = null;
		numberB = null;
	}
}

btnClearAll.onclick = () => {
	numberA = null;
	operation = null;
	inputDisplay.value = null;
}

btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, -1);
}

btnDecimal.onclick = () => {
	if(!inputDisplay.value.includes(".")){
		inputDisplay.value = inputDisplay.value + btnDecimal.innerText;
	}
}

